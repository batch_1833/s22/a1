// Translate Data models into codes

// Users Collection (Users.json)
{
		
	"_id":"user001",
	"firstName": "John",
	"lastName" : "Smith",
	"email" : "johnsmith@mail.com",
	"password" : "johnsmith123",
	"isAdmin" : true,
	"mobileNumber" : "09123456789",

}
{
	
	"_id":"user002",
	"firstName": "Will",
	"lastName" : "Smith",
	"email" : "willsmith@mail.com",
	"password" : "willsmith111",
	"isAdmin" : false,
	"mobileNumber" : "09471644552",

}
// Product Collection 

{
	"_id": "product001",
	"name" : "Spam",
	"description" : " canned cooked pork",
	"price" : 1000,
	"stocks": 150,
	"isActive": true,
	"SKU" : "Yimweos123" 
}

{
	"_id": "product002",
	"name" : "Corned Beef",
	"description" : " canned",
	"price" : 500,
	"stocks": 120,
	"isActive": true,
	"SKU" : "Ynsdk132" 
}

// OrderProducts Collection 

{

	"_id": "orderProducts001",
	"userId": "user002",
	"orderID": "order001",
	"productID": "product001",
	"Quantity": "2",
	"Price" : 1000,
	"Subtotal": 2000

}
{

	"_id": "orderProducts002",
	"userId": "user001",
	"orderID": "order002",
	"productID": "product002",
	"Quantity": "2",
	"Price" : 500,
	"Subtotal": 1000

}

// Orders Collection 
{

	"_id": "order001",
	"userId": "user002",
	"transactionDate": "2022-06-10T15:00:00.00Z",
	"status": "Pending",
	"Total": 2000,
	

}


